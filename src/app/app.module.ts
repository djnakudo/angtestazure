import { FormModule } from './form/form.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxMaskModule, IConfig} from 'ngx-mask'
import { AppComponent } from './app.component';

import {ReactiveFormsModule} from '@angular/forms'
 


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgxMaskModule.forRoot(),
    FormModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
