import { FormService } from './form.service';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup,Validators, FormControl} from '@angular/forms';
import { PhoneValidationFactory } from './phone/phone.validation.factory';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
 
  operators = []
  currentRefillValues = []
  profileForm : FormGroup;


  constructor(private fb : FormBuilder,private formService : FormService) { }
  onSubmit(){

  }
  handleChangeOperation(){
    const foneControl = <FormControl>this.profileForm.get('fone');
    this.profileForm.get('operator').valueChanges.subscribe(operatorId=>{
      this.currentRefillValues = this.operators.find(op=>op.id==operatorId).chargeValues || [];
      const newValidation = new PhoneValidationFactory(foneControl);
      newValidation.setValidation(operatorId);
    })    
  }


  ngOnInit() {
    this.operators = this.formService.getOperators();
    this.profileForm= this.fb.group({
      firstName:[''],
       lastName: [''],
       fone: [''],
       operator: [''],
       refillValue:['',[Validators.required]]
    });
    this.handleChangeOperation();
  }

}
