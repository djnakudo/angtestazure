import { FormService } from './form.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComponent } from './form.component';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

describe('FormComponent', () => {
  let component: FormComponent;
  let service: FormService;
  let fixture: ComponentFixture<FormComponent>;
  let operators = [];


  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule,
      platformBrowserDynamicTesting());
  });



  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [FormComponent]
    });

    // create component and test fixture
    fixture = TestBed.createComponent(FormComponent);

    // get test component from the fixture
    component = fixture.componentInstance;
    component.ngOnInit(); 
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should change operation taxes to correct one',

    async () => {
    
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      const selector = compiled.querySelector('select.custom-select');
      selector.value = selector.options[2].value;
      const optionSelected = selector.options[2].text;
      selector.dispatchEvent(new Event('changes'));
      fixture.detectChanges();
      expect(optionSelected).toEqual(component.operators[1].value)
      


    });

    

});
