import { Injectable } from '@angular/core';
import {Form} from './form.interface'
@Injectable({
  providedIn: 'root'
})
export class FormService {
  forms : Form[]=[
  {id:'Oi',
   value:'Oi',
   chargeValues:[25,30]
},
{id:'Vivo',
   value:'Vivo',
   chargeValues:[11,22]
},
{id:'Claro',
   value:'Claro',
   chargeValues:[25,30,40]
},

  ]
  constructor() { }


  
  getOperators(){
    try {
      return this.forms;
    } catch (error) {
      throw new Error(error)
    }
   
  }
}
