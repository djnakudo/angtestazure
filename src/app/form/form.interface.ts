export interface Form {

    id: string,
    value: string,
    chargeValues: number[]

}