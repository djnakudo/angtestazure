import { TestBed } from '@angular/core/testing';

import { FormService } from './form.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

describe('FormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [FormService] })
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule,
       platformBrowserDynamicTesting());

  });

  it('should be created', () => {
    const service: FormService = TestBed.get(FormService);
    expect(service).toBeTruthy();
  });
  it('should return array of operations', () => {
    const service: FormService = TestBed.get(FormService);
    const operations = service.getOperators();
    expect(operations).toBeDefined();
    operations.forEach(operation => {
      expect(operation.id).toBeDefined()
      expect(operation.value).toBeDefined()
      expect(operation.chargeValues).toBeDefined()
    });

  })
});
