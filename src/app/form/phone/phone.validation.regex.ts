import { FormControl, Validators } from '@angular/forms';
import { ValidField } from '../validators.recharge';

const patterns = {
    'Oi':{
        function:'setValidators',
        validators:[Validators.required,ValidField('fone',/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/ )]
    },
    'Vivo':{
        function:'setValidators',
        validators:[Validators.required,ValidField('fone',/^[+ 0-9]{8}$/ )]
    },
    'Claro':{
        function:'setValidators',
        validators:[Validators.required,ValidField('fone',/^[+ 0-9]{7}$/ )]
    }
    
}


export const regexDeploy =(formControl: FormControl,operationId:string)=> {
    const patternObj = patterns[operationId] || {function:'clearValidators',validators:null};
    formControl[patternObj.function](patternObj.validators);
    formControl.updateValueAndValidity();
}
