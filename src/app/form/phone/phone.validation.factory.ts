import {Injectable} from '@angular/core'
import { FormControl,Validators } from '@angular/forms';
import {regexDeploy} from './phone.validation.regex'
import { ValidField } from '../validators.recharge';
@Injectable({
    providedIn: 'root'
  })
export class PhoneValidationFactory{
    formControl : FormControl;
    constructor(form : FormControl){
        this.formControl = form;
        this.formControl.clearValidators()
        this.formControl.updateValueAndValidity();
    }
    setValidation(operationId:string){
        regexDeploy(this.formControl,operationId)
    }



}