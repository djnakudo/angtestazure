import { AbstractControl, ValidatorFn } from '@angular/forms';
import { isRegExp } from 'util';



export function ValidField(formControlName: string, regex: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (control.value !== undefined && isRegExp(regex) && !regex.test(control.value)) {
            let obj = {};
            obj[`${formControlName}validator`] = true;
            return obj;
        }
        return null;
    }


}



