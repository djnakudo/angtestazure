import { BrowserModule } from '@angular/platform-browser';
import { FormService } from './form.service';
import { FormComponent } from './form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [FormComponent],
  providers:[FormService],
  exports:[FormComponent],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    CommonModule
  ]
})
export class FormModule { }
